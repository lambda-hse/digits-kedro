import os.path
from dataclasses import dataclass

import torch as t
from kedro.io import AbstractDataSet

from .mnist import Dataset as DatasetMNIST
from .svhn import Dataset as DatasetSVHN


@dataclass(frozen=True)
class DatasetName:
    mnist = 'mnist'
    svhn = 'svhn'


class ImagesDataset(AbstractDataSet):
    def __init__(self, dataset_name: str):
        super().__init__()
        self.dataset_name = dataset_name.lower()

    def _load(self):
        dataroot = os.path.join('data', '01_raw')
        if self.dataset_name == DatasetName.mnist:
            ds = DatasetMNIST(dataroot=os.path.join(dataroot, 'mnist'))
            return ds
        elif self.dataset_name == DatasetName.svhn:
            ds = DatasetSVHN(dataroot=os.path.join(dataroot, 'svhn'))
            return ds

    def _save(self, tensor: t.Tensor) -> None:
        raise Exception('Not implemented')

    def _exists(self) -> bool:
        raise Exception('Not implemented')

    def _describe(self):
        return dict()
