from kedro.pipeline import Pipeline
from kedro.pipeline import node

from digits.pipelines.nodes import train, test, report


def create_pipeline(**kwargs):
    return Pipeline([
        node(
            train,
            inputs=[
                'images_dataset',
                'parameters_dotmap',
            ],
            outputs=['classifier-state_dict'],
        ),

        node(
            test,
            inputs=[
                'images_dataset',
                'classifier-state_dict',
                'parameters_dotmap',
            ],
            outputs=[
                'accuracy_train',
                'accuracy_test',
            ]
        ),

        node(
            report,
            inputs=[
                'parameters_dotmap',
                'accuracy_train',
                'accuracy_test',
            ],
            outputs=None,
        )
    ])
