import json
import os
from collections import defaultdict

import numpy as np
import torch
import torch.utils
import torch.utils.data
from tqdm import tqdm

from ..models.digits_classifier import DigitClassifier
from ..utils import available
from ..utils.logger import get_logger
from ..utils.report import as_tabular, as_table


def train(dataset, params):
    log = get_logger(params.logger)

    device = torch.device(params.device)
    torch.manual_seed(params.seed)

    trainloader = torch.utils.data.DataLoader(dataset.train_set, batch_size=params.batch_size, shuffle=True)

    model = available.models[params.model_name].Model(dataset.train_set[0][0].shape).to(device)
    clf = DigitClassifier(model, device=device)

    if not params.quiet:
        arguments = dict(seed=params.seed, n_epoches=params.n_epoches, batch_size=params.batch_size)
        print('Training {model}({arguments}) on {dataset}'.format(
            model=params.model_name,
            dataset=dataset.name,
            arguments=', '.join(['%s = %s' % (k, v) for k, v in arguments.items()]),
        ))

    losses = clf.fit(trainloader, n_epoches=params.n_epoches, progress=None if params.quiet else tqdm)

    parameters_path = os.path.join('data', '06_models', 'model_parameters.pt')
    torch.save(clf.classifier.state_dict(), parameters_path)
    if not params.quiet:
        print('  saving to {parameters_path}'.format(parameters_path=parameters_path))
    log.log_losses(dataset.name, params.model_name, losses)

    return [clf.classifier.state_dict()]


def test(
        dataset,
        state_dict,
        params,
):
    log = get_logger(params.logger)

    device = torch.device(params.device)

    trainloader = torch.utils.data.DataLoader(dataset.train_set, batch_size=params.batch_size, shuffle=True)
    testloader = torch.utils.data.DataLoader(dataset.test_set, batch_size=params.batch_size, shuffle=True)

    model = available.models[params.model_name].Model(dataset.train_set[0][0].shape).to(device)
    clf = DigitClassifier(model, device=device)

    clf.classifier.load_state_dict(state_dict)

    predictions_train, true_train = clf.predict(trainloader)
    predictions_test, true_test = clf.predict(testloader)

    accuracy_train = np.mean(np.argmax(predictions_train, axis=1) == true_train)
    accuracy_test = np.mean(np.argmax(predictions_test, axis=1) == true_test)

    log.log_metrics(dataset.name, params.model_name, accuracy_train=accuracy_train, accuracy_test=accuracy_test)

    return [accuracy_train, accuracy_test]


def report(
        params,
        # Параметры для синхронизации: `report` должен выполниться после `test`
        # т.к. `log.log_metrics` пишет в файл, а не передаёт значение в графе
        accuracy_train, accuracy_test,
):
    results = defaultdict(dict)

    for item in os.listdir(params.report.dir):
        try:
            if not item.endswith('.json'):
                continue
            path = os.path.join(params.report.dir, item)
            with open(path, 'r') as f:
                record = json.load(f)

            dataset = record['dataset']
            model = record['model']
            results[dataset][model] = dict(
                accuracy_train=record['accuracy_train'],
                accuracy_test=record['accuracy_test'],
            )
        except:
            import warnings
            import traceback
            warnings.warn(traceback.format_exc())

    for dataset in results:
        try:
            path = os.path.join(params.report.output_dir, '{dataset}.tex'.format(dataset=dataset))
            with open(path, 'w') as f:
                f.write(as_tabular(results[dataset]))
        except:
            import warnings
            import traceback
            warnings.warn(traceback.format_exc())

        try:
            path = os.path.join(params.report.output_dir, '{dataset}.txt'.format(dataset=dataset))
            table = as_table(results[dataset])
            with open(path, 'w') as f:
                f.write(table)

            if not params.quiet:
                print(dataset)
                print(table)
        except:
            import warnings
            import traceback
            warnings.warn(traceback.format_exc())
