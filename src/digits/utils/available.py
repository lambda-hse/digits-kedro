import digits.models as models

__all__ = [
    'models',
]

models = {
    attr: getattr(models, attr)
    for attr in models.__all__
}
