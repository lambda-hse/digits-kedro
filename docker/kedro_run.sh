# Run this script from root project directory: ./docker/run.sh

docker build -f docker/Dockerfile -t digits-kedro . && \
docker run -it --rm \
    -v $PWD:/root/app \
    digits-kedro \
    kedro run
